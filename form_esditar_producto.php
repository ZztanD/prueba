<?php

$id = $_GET['id'];
$objeto = new Conexion();
$conexion = $objeto->get_conexion();
$conexion->exec("SET CHARACTER SET utf8");
$sql2="SELECT * FROM inventario WHERE id_inventario = $id";
$resultado2=$conexion->prepare($sql2);
$resultado2->execute(array());
$registros2 =$resultado2->fetch (PDO::FETCH_ASSOC);
$codigo = $registros2['codigo_barra'];
$existencia = $registros2['existencia'];
$descripcion = $registros2['descripcion'];
$precio_costo = $registros2['precio_costo'];//tabla precios
$estado = $registros2['estado'];
$idcategoria_viene = $registros2['idcategoria'];
$nombre_producto = $registros2['nombre_producto'];//ok

$sql3="SELECT * FROM precios WHERE id_inventario = $id";
$resultado3=$conexion->prepare($sql3);
$resultado3->execute(array());
$registros3 =$resultado3->fetch (PDO::FETCH_ASSOC);
$precio_venta = $registros3['precio_venta1'];
$precio_obra = $registros3['precio_mano1'];

$sql4="SELECT * FROM categorias WHERE idcategoria = $idcategoria_viene";
$resultado4=$conexion->prepare($sql4);
$resultado4->execute(array());
$registros4 =$resultado4->fetch (PDO::FETCH_ASSOC);
$nombre_categoria_viene = $registros4['nombre_categoria'];



if (isset($_GET['msj'])) {
    $mensaje = $_GET['msj'];
}else{
  $mensaje = "";
}

if ($mensaje == "ok") {
?>

    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Registro Almecenado!</strong> El registro ha sido alamacenado correctamente.
    </div>
<?php
}

if ($mensaje == "ok2") {
?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Registro Actualizado!</strong> El registro ha sido alamacenado correctamente.
    </div>
<?php
}

if ($mensaje == "ok3") {
?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Registro Eliminado!</strong> El registro ha sido eliminado correctamente.
    </div>
<?php
}
if ($mensaje == "rep") {
?>
    <div class="alert alert-warning  alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Registro no almacenado!</strong> No se puede ingresar el registro porque el codigo introducido ya existe en la base de datos.
    </div>
<?php
}
         $list_categoria='';
         $objeto = new Conexion();
         $conexion = $objeto->get_conexion();
         $conexion->exec("SET CHARACTER SET utf8");
         $sql="SELECT * FROM categorias ";
         $resultado=$conexion->prepare($sql);
         $resultado->execute(array());
         while($registros =$resultado->fetch (PDO::FETCH_ASSOC)){
         $idcategoria =$registros['idcategoria'];
         $nombre_categoria =$registros['nombre_categoria'];
         $list_categoria .="<option value='".$idcategoria."'>".$nombre_categoria."</option>";
      }
?>

<script type="text/javascript">


function buscar() {
    var textoBusqueda = $("input#codigo").val();
    var tipo_formulario = "editar";
    var codigo_antiguo = "<?php echo $codigo?>";
     if (textoBusqueda != "") {
        $.post("fetch.php", {valorBusqueda: textoBusqueda, formulario:tipo_formulario, codigo_primero:codigo_antiguo}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
           
        }); 
    } else { 
       $("#resultadoBusqueda").html('');
  };
};

</script>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Producto<small>Control Activo</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="producto/actualizar_producto.php" method="POST">

                     
                      
                
                    
                  <div class="row">

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group" >
                    <label >Código *</label>
                    <input type="number" placeholder="Código" onKeyUp="buscar();" name="codigo" id="codigo" class="form-control" id="codigo" value="<?php echo $codigo?>" required>
                  </div>

                  <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                    <label >Nombre del producto</label>
                    <input type="text" maxlength="50" placeholder="Nombre del producto" name="nombre_producto" id="nombre_producto" value="<?php echo $nombre_producto?>" class="form-control" required>
                  </div>

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Existencias</label>
                    <input type="number" id="existencia" value="<?php echo $existencia?>" placeholder="Existencias" name="existencia" class="form-control">
                  </div>

                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                    <label >Descripción del producto</label>
                    <input type="text" id="descripcion" maxlength="150" placeholder="Descripción del producto" name="descripcion" value="<?php echo $descripcion?>" class="form-control" required>
                  </div>

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Precio Costo</label>
                    <input type="text" id="precio_costo" placeholder="Precio Costo" onkeypress="return soloNumero(event)" data-validate-length-range="6" data-validate-words="2" class="form-control" name="precio_costo" value="<?php echo $precio_costo?>">
                  </div>
                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Estado en inventario</label>
                    <select class="form-control" name="estado" id="estado"  required>
                            <option value="<?php echo $estado?>"><?php echo $estado?></option>
                            <option value="Activo">Activo</option>
                            <option value="Inactivo">Inactivo</option>
                    </select>
                  </div>

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Categoria</label>
                    <select class="form-control" id="idcategoria" name="idcategoria"  required>
                            <option value="<?php echo $idcategoria_viene?>"><?php echo $nombre_categoria_viene ?></option>
                            <?php echo $list_categoria; ?>
                    </select>
                  </div>

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Precio de Venta</label>
                    <input type="text" id="precio_venta" placeholder="Precio Venta" onkeypress="return soloNumero(event)" name="precio_venta" value="<?php echo $precio_venta?>" class="form-control" required>
                  </div>

                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                    <label >Precio Mano de Obra</label>
                    <input type="text" id="precio_obra" placeholder="P. / Mano de Obra" onkeypress="return soloNumero(event)" name="precio_obra" value="<?php echo $precio_obra?>" class="form-control" required>
                  </div>

                  <input type="hidden" name="id" value="<?php echo $id?>">





                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="pagina_producto.php?view=true" class="btn btn-primary" type="button">Cancelar</a>
                          
                          <button type="submit" name="actualizar_producto" id="actualizar_producto" class="btn btn-success">Actualizar</button>
                        </div>
                      </div>
                      <div id="resultadoBusqueda">
                        

                      </div>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           